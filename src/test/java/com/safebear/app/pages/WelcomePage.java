package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class WelcomePage {

    WebDriver driver;

    @FindBy(linkText = "Login") // Annotated variable
    WebElement loginlink;

    public WelcomePage (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrctPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public boolean clickOnLogin(LoginPage loginPage) {
        loginlink.click();
        return loginPage.checkCorrectPage();
    }
}
