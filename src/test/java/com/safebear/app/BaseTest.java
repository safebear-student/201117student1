package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import utils.Utils;

import javax.swing.text.Utilities;
import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class BaseTest {

    private WebDriver driver;
    private Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp() {

        utility = new Utils();
        driver = utility.getDriver();
        driver.get(utility.getUrl());
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown(){
        try {
            TimeUnit.SECONDS.sleep(2);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        driver.quit();
    }
}
