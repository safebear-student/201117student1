package com.safebear.app;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {

        String username = "testuser";
        String password = "testing";

        //Step 1 - Confirm 'Welcome' page
        assertTrue(welcomePage.checkCorrctPage());
        //Step 2 - Navigate to 'Login' page and confirm correct page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 -
        assertTrue(loginPage.login(this.userPage, username, password));
        //Step 4 -
        assertTrue(userPage.clickOnLogOut(this.welcomePage));
    }
}
